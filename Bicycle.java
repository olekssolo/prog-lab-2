// Oleksandr Sologub, 2034313
public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }
    public String toString() {
        return "Manufacturer: " + manufacturer + ", gears: " + numberGears + ", speed: " + maxSpeed;
    }
}