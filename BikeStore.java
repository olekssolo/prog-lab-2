//Oleksandr Sologub, 2034313
public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bicycles = new Bicycle[4];
        bicycles[0] = new Bicycle("mazda", 5, 120);
        bicycles[1] = new Bicycle("suzuki", 10, 150);
        bicycles[2] = new Bicycle("chevrolet", 15, 125);
        bicycles[3] = new Bicycle("honda", 20, 200);

        for (int i=0; i<4; i++) {
            System.out.println(bicycles[i].toString());
        }
    }
}
